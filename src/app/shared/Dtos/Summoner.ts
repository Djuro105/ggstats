export interface Summoner {
    id: any
    accountId: any
    puuid: any
    name: string
    profileIconId: any
    revisionDate: any
    summonerLevel: any
}