export interface Match {
    platformId: any
    gameId: any
    champion: any
    queue: string
    season: any
    timestamp: any
    role: any
    lane: any
}