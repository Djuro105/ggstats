import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Summoner } from '../Dtos/Summoner';
import { Match } from '../Models/Match';
import { Matches } from '../Dtos/Matches';



@Injectable({
  providedIn: 'root'
})


export class ApiHelperService {
  APIKEY: string = "RGAPI-77ec580c-34c9-43a6-8306-38d3d437fbd3";

  summoner: Summoner;
/* 
  BestDjuroEUNE = "0XVsNo8ge9rGx0VSXSL5TPTPUtBEOKDjBg-XsGXzvbQDWQ";
  Oxa = "VxCexpOfYR8dZ0GVwtqvr66HjeovbbKISXONSUNjsotMbEw";
*/
  constructor(private http: HttpClient) { }

  getPlayerByName(sumName: string) : Observable<Summoner>{
    return this.http.get<Summoner>("/api/lol/summoner/v4/summoners/by-name/"+sumName+"?api_key="+this.APIKEY);
  }

  getMatchesByAccountId(accId: string) : Observable<Matches> {
    return this.http.get<Matches>("/api/lol/match/v4/matchlists/by-account/"+accId+"?api_key="+this.APIKEY);
  }


  
}
