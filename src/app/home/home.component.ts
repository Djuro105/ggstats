import { Component, OnInit } from '@angular/core';
import { ApiHelperService } from '../shared/Services/api-helper.service';
import { Summoner } from '../shared/Dtos/Summoner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private apiHelper: ApiHelperService, private router: Router) { }

  ngOnInit(): void {}


  loadSummonerProf(sumName: string): void {
    this.router.navigate(['/summoner/' + sumName]);
  }

}
