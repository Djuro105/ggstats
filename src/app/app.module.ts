import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms' 
import { Routes, RouterModule } from "@angular/router"

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ApiHelperService } from './shared/Services/api-helper.service';
import { HttpClientModule } from '@angular/common/http';
import { SummonerComponent } from './summoner/summoner.component';

import { CommonModule } from '@angular/common';

const appRoutes: Routes = [
  { path: "", component: HomeComponent},
  { path: "summoner/:sumName", component: SummonerComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    SummonerComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
    
  ],
  providers: [ApiHelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
