import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Summoner } from '../shared/Dtos/Summoner';
import { ApiHelperService } from '../shared/Services/api-helper.service';
import { Router, ActivatedRoute } from '@angular/router';

import { Matches } from '../shared/Dtos/Matches';
import { Match } from '../shared/Models/Match';

@Component({
  selector: 'app-summoner',
  templateUrl: './summoner.component.html',
  styleUrls: ['./summoner.component.css']
})


export class SummonerComponent implements OnInit {


  constructor(
    private apiHelper: ApiHelperService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute
    ) { }
    
  summoner: Summoner;

  listMatches: Match[];

  sumName: string;

  ngOnInit(): void {
    
    // dohvati sumName iz url-a, te pozovi funkciju za dohvat summonera preko name-a
    this.sumName = this.activatedRoute.snapshot.params["sumName"];
    this.getSummonerByName(this.sumName);
    //this.getMatchesOfPlayer("VxCexpOfYR8dZ0GVwtqvr66HjeovbbKISXONSUNjsotMbEw"); // fiksirano radi
    //this.getMatchesOfPlayer(this.summoner.accountId); // pukne jer jos ne loada accId summonera
  }

  getSummonerByName(sumName: string): void{
    this.apiHelper.getPlayerByName(sumName)
      .subscribe(summoner => {
        this.summoner = summoner;
        console.log("ID:  " + this.summoner.id);
        console.log("ACC ID:  " + this.summoner.accountId);
        console.log("NAME:  " + this.summoner.name);
        console.log("PROFILE ICON:  " + this.summoner.profileIconId);
        console.log("PUUID:  " + this.summoner.puuid);
        console.log("REVISION DATE:  " + this.summoner.revisionDate);
        console.log("SUMMONER LEVEL:  " + this.summoner.summonerLevel);

        this.getMatchesOfPlayer(this.summoner.accountId); // vjerojatno nije pametno ovdje to napraviti ali radi
      });
  }

  getMatchesOfPlayer(accId: string): void{
    this.apiHelper.getMatchesByAccountId(accId)
      .subscribe(matchesObjekt => {
        this.listMatches = matchesObjekt.matches;
        console.log(this.listMatches);
        console.table(this.listMatches);
        
      });

  }


}

  